import React, { Component } from 'react'
import Footer from './shared/Footer'
import Header from './shared/Header'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

export default class TestOnline extends Component {
    render() {
        return (
            <div className="test-page">
                <Header />

                <div className="main-content">
                    <div className="container-fluid">
                        <div className="main-content__child">
                            <div className="row">
                                <div className="col-lg-2 active-pc aside aside-left">
                                    <ul className="aside-menu">
                                        {/* <li>
                                            <a href title className="menu-title"><i className="fa fa-home" aria-hidden="true" />Home</a>
                                        </li>
                                        <li>
                                            <a href title className="menu-title"><i className="fa fa-book" aria-hidden="true" />Khóa
                                            học của tôi</a>
                                        </li>
                                        <li>
                                            <a href title className="menu-title"><i className="fa fa-video-camera" aria-hidden="true" />Livesreams</a>
                                        </li>
                                        <li>
                                            <a href title className="menu-title"><i className="fa fa-pencil-square-o" aria-hidden="true" />Test online</a>
                                        </li>
                                        <li>
                                            <a href title className="menu-title"><i className="fa fa-history" aria-hidden="true" />Lịch
                                            sử học tập</a>
                                        </li> */}
                                    </ul>
                                </div>

                                <div className="col-lg-8 col-xs-12 test-content-common">
                                    <div className="sec-route">
                                        <div className="sec-head sec-head-common-1">
                                            <div className="heading">
                                                Luyện tập TOEIC theo level
                                            </div>
                                            <div className="sub-heading">Chọn level bạn muốn test để bắt đầu</div>
                                        </div>
                                        <div className="sec-body">
                                            <div className="box-progress">
                                                <span className="dot dot-1" />
                                                <span className="dot dot-2" />
                                                <span className="dot dot-3" />
                                            </div>
                                            <div className="box-level">
                                                <div className="level-child">
                                                    <div className="text">LEVEL</div>
                                                    <div className="point">
                                                        250 - 500
                                                    </div>
                                                </div>
                                                <div className="level-child">
                                                    <div className="text">LEVEL</div>
                                                    <div className="point">
                                                        500 - 750
                                                    </div>
                                                </div>
                                                <div className="level-child">
                                                    <div className="text">LEVEL</div>
                                                    <div className="point">
                                                        750 - 990
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="sec-mini-test">
                                        <div className="sec-head sec-head-common-1">
                                            <div className="heading">
                                                Test TOEIC online
                                            </div>
                                        </div>
                                        <div className="sec-body">
                                            <div className="row">

                                                <div className="col-lg-6">
                                                    <div className="mini-test">
                                                        <div className="collapse-title" >full test</div>
                                                        <div className="collapse-content">
                                                            <ul className="list-photo">
                                                                <li>
                                                                    <Link to="/fulltest" className="photo-title">
                                                                        BÀI THI THỬ TOEIC ONLINE - Mã đề 15436
                                                                    </Link>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                {/* <div className="col-lg-6">
                                                    <div className="mini-test">
                                                        <div className="collapse-title" data-toggle="collapse" data-target="#mini-test-1">mini test</div>
                                                        <div id="mini-test-1" className="collapse collapse-content">
                                                            <ul className="list-photo">
                                                                <li>
                                                                    <a href title className="photo-title">1. PART 1 - photo - Skill Test_1</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div> */}

                                                {/* <div className="col-lg-6">
                                                    <div className="mini-test">
                                                        <div className="collapse-title" data-toggle="collapse" data-target="#mini-test-2">skill test</div>
                                                        <div id="mini-test-2" className="collapse collapse-content">
                                                            <ul className="list-photo">
                                                                <li>
                                                                    <a href title className="photo-title">1. PART 1 - photo - SkillTest_1</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div> */}


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Footer />
            </div>
        )
    }
}
