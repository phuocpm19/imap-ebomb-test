import React, { Component } from 'react'
import Header from '../shared/Header'
// import Footer from '../shared/Footer'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Modal, Tabs, Tab } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import SecHead from '../shared/SecHead';
// import QuestionItem from '../shared/QuestionItem';
import TabListQuestion from '../shared/TabListQuestion';
import ListPart from '../shared/ListPart';
import HeaderMobile from '../shared/HeaderMobile';
import Axios from 'axios';
// import QuestionItemPartOne from '../shared/QuestionItemPartOne';

export default class Part1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: false,
            keyTabs: 'home',
            statusTableQuestion: false,
            data_list_question_part_one: []
        }
    }

    _getDataListQuestionPartOne = async () => {
        if (this.state.is_loading) { return }

        this.setState({
            is_loading: true
        })
        const response = await Axios.get("https://5f1aab8a610bde0016fd2df3.mockapi.io/listQuestionPartOne")
        console.debug(response)
        if (!response.data?.error) {
            this.setState({
                is_loading: false,
                data_list_question_part_one: response.data
            })
        } else {
            console.debug("Get fail")
            this.setState({
                is_loading: false
            })
        }
    }

    componentDidMount() {
        this._getDataListQuestionPartOne()
    }

    closeModal = () => {
        this.setState({
            showModal: false
        });
    }

    openModal = () => {
        this.setState({
            showModal: true
        });
    }

    onHandleToggle = () => {
        this.setState({
            statusTableQuestion: !this.state.statusTableQuestion
        });
    }

    onHandleChoice = () => {
        console.log(111);
        // console.log(this.state.data_list_question_part_one);
        // let target = this.state.data_list_question_part_one[listChoice];
        // console.log(target);
    }

    render() {
        const { showModal, statusTableQuestion } = this.state;

        return (
            <div className="test-page">
                <Header />

                <HeaderMobile />

                <section className="main-content bg-f3">
                    <div className="test-detail test-detail-pc active-pc">
                        <div className="container-fluid">
                            <div className="main-content__child">
                                <div className="row">
                                    <div className="col-lg-2 aside aside-left" />

                                    <div className="col-lg-8 test-content-common test-content-common-1">
                                        <div className="test-part-common">
                                            <SecHead
                                                heading="Part I: Picture Description"
                                                subHeading="Look at the picture and listen to the sentences. Choose
                                                the sentence that best describes the picture:"
                                            />
                                            <div className="sec-body sec-body-common-2">
                                                {
                                                    this.state.data_list_question_part_one.map((item, index) => {
                                                        return <div className="sec-body sec-body-common-2" key={index}>
                                                            <div className="box-question">
                                                                <div className="box-question__title">
                                                                    Question {item.id}:
                                                                        </div>
                                                                <div className="box-picture">
                                                                    <div className="image img-scaledown">
                                                                        <img src={item.srcImg} alt="anh" />
                                                                    </div>
                                                                </div>
                                                                <div className="box-audio">
                                                                    <audio controls>
                                                                        <source src={item.srcAudio} type="audio/ogg" />
                                                                        <source src={item.srcAudio} type="audio/mpeg" />
                                                                                Your browser does not support the audio element.
                                                                            </audio>
                                                                </div>
                                                                <div className="box-question__content">
                                                                    <ul className="list-question">
                                                                        {
                                                                            item.listChoice.map((item2, index2) => {
                                                                                return <li key={index2}>
                                                                                    <div className="item">
                                                                                        <div className="item__choice" onClick={this.onHandleChoice}>
                                                                                            {item2.itemChoice}
                                                                                        </div>
                                                                                        <div className="item__content" onClick={this.onHandleChoice}>
                                                                                            {item2.itemContent}
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            })
                                                                        }
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-2 aside aside-right" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="test-detail test-detail-mb active-mb">
                        <div className="container">
                            <div className="test-detail-mb-content">
                                <div className="r-tab-custom">
                                    <Tabs
                                        id="controlled-tab-example"
                                        activeKey={this.state.keyTabs}
                                        onSelect={keyTabs => this.setState({ keyTabs })}
                                        className="r-tab-list"
                                    >
                                        <Tab eventKey="home" title="Bài Test" className="r-tab-item">
                                            <div className="r-tab-content tab-1">
                                                <div className="test-part-common">
                                                    <SecHead
                                                        heading="Part I: Picture Description"
                                                        subHeading="Look at the picture and listen to the sentences. Choose
                                                        the sentence that best describes the picture:"
                                                    />
                                                    {
                                                        this.state.data_list_question_part_one.map((item, index) => {
                                                            return <div className="sec-body sec-body-common-2" key={index}>
                                                                <div className="box-question">
                                                                    <div className="box-question__title">
                                                                        Question {item.id}:
                                                                        </div>
                                                                    <div className="box-picture">
                                                                        <div className="image img-scaledown">
                                                                            <img src={item.srcImg} alt="anh" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="box-audio">
                                                                        <audio controls>
                                                                            <source src={item.srcAudio} type="audio/ogg" />
                                                                            <source src={item.srcAudio} type="audio/mpeg" />
                                                                                Your browser does not support the audio element.
                                                                            </audio>
                                                                    </div>
                                                                    <div className="box-question__content">
                                                                        <ul className="list-question">
                                                                            {
                                                                                item.listChoice.map((item2, index2) => {
                                                                                    return <li key={index2}>
                                                                                        <div className="item">
                                                                                            <div className="item__choice">{item2.itemChoice}</div>
                                                                                            <div className="item__content">{item2.itemContent}</div>
                                                                                        </div>
                                                                                    </li>
                                                                                })
                                                                            }
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        </Tab>

                                        <Tab eventKey="profile" title="Bảng câu hỏi" className="r-tab-item">
                                            <div className="r-tab-content tab-2">
                                                <TabListQuestion />
                                            </div>
                                        </Tab>

                                        <Tab eventKey="contact" title="Xem các Part" className="r-tab-item">
                                            <div className="r-tab-content tab-3">
                                                <ListPart />
                                            </div>
                                        </Tab>
                                    </Tabs>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="boxTime" className="box-time box-time-pc active-pc">
                    <div className="container-fluid">
                        <div className="row row-mg-0">
                            <div className="col-lg-2 col-pd-0" />
                            <div className="col-lg-8 col-pd-0">
                                <div className="box-time-content">
                                    <div className="box-time-content__left">
                                        <div className="box-button">
                                            <button id="buttonClickShowTableQuestion" type="button" className="btn btn-primary ml0" onClick={this.onHandleToggle}>Bảng
                                                câu hỏi</button>
                                        </div>
                                    </div>
                                    <div className="box-time-content__center">
                                        <i className="fa fa-clock-o" aria-hidden="true" />
                                        <span id="ten-countdown" />
                                    </div>
                                    <div className="box-time-content__right">
                                        <div className="box-button">
                                            {/* <a className="btn btn-cyan">Next Question<i className="fa fa-chevron-circle-right" aria-hidden="true" /></a> */}
                                            <Link to="/part-2" className="btn btn-cyan">Next Part<i className="fa fa-arrow-circle-right" aria-hidden="true" /></Link>
                                            <Button className="btn btn-default" onClick={this.openModal}>
                                                Nộp bài
                                            </Button>
                                        </div>
                                    </div>
                                    <div id="tableQuestion" className={statusTableQuestion === false ? 'table-question dp-none' : 'table-question dp-block'} >
                                        <div className="title">Bảng câu hỏi</div>
                                        <div className="content">
                                            <ul className="list-number">
                                                <li><span className="checked">1</span></li>
                                                <li><span>2</span></li>
                                                <li><span>3</span></li>
                                                <li><span>4</span></li>
                                                <li><span>5</span></li>
                                                <li><span>6</span></li>
                                                <li><span>7</span></li>
                                                <li><span>8</span></li>
                                                <li><span>9</span></li>
                                                <li><span>10</span></li>
                                                <li><span>11</span></li>
                                                <li><span>12</span></li>
                                                <li><span>13</span></li>
                                                <li><span>14</span></li>
                                                <li><span>15</span></li>
                                                <li><span>16</span></li>
                                                <li><span>17</span></li>
                                                <li><span>18</span></li>
                                                <li><span>19</span></li>
                                                <li><span>20</span></li>
                                                <li><span>21</span></li>
                                                <li><span>22</span></li>
                                                <li><span>23</span></li>
                                                <li><span>24</span></li>
                                                <li><span>25</span></li>
                                                <li><span>26</span></li>
                                                <li><span>27</span></li>
                                                <li><span>28</span></li>
                                                <li><span>29</span></li>
                                                <li><span>30</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2" />
                        </div>
                    </div>
                </section>

                <section id="boxTimeMb" className="box-time box-time-mb active-mb">
                    <div className="box-time-mb-content">
                        <div className="time-total">
                            <i className="fa fa-clock-o" aria-hidden="true" />
                            <span id="ten-countdown-mb" />
                        </div>
                        <div className="box-button">
                            {/* <a className="btn btn-cyan">Next Question<i className="fa fa-chevron-circle-right" aria-hidden="true" /></a> */}
                            <Link to="/part-2" className="btn btn-cyan">Next Part<i className="fa fa-arrow-circle-right" aria-hidden="true" /></Link>
                            <Button className="btn btn-default" onClick={this.openModal}>Nộp bài</Button>
                        </div>
                    </div>
                </section>

                <Modal show={showModal} onHide={this.closeModal} className="modal-submit">
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <button type="button" className="close"></button>
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div>
                            <div className="title">Chức mừng bạn đã hoàn thành bài thi</div>
                            <div className="sub-title">Interview Skills Test 1</div>
                            <p className="text">Điểm của bạn</p>
                            <div className="circle">
                                <span className="point">500/990</span>
                            </div>
                            <p className="text">Đáp án đúng: 0 / 30</p>
                            <p className="text">Số câu hoàn thành: 0 / 30 câu</p>
                            <div className="box-button">
                                {/* <a className="btn btn-blue-ebomb">Nhận tư vấn lộ trình học</a> */}
                                {/* <a className="btn btn-blue-ebomb">Làm lại bài test</a> */}
                                <Link to="/fulltest" className="btn btn-blue-ebomb">Làm lại bài test</Link>
                                {/* <a className="btn btn-blue-ebomb">Xem đáp án</a> */}
                            </div>
                        </div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.closeModal}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        )
    }
}
