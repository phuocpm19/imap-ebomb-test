import React, { Component } from 'react'
import Header from '../shared/Header';
import { Button, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import SecHead from '../shared/SecHead';

export default class Part4 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: false,
            statusListQuaestion: false
        }
    }

    closeModal = () => {
        this.setState({
            showModal: false
        });
    }

    openModal = () => {
        this.setState({
            showModal: true
        });
    }

    onHandleToggle = () => {
        this.setState({
            statusListQuaestion: !this.state.statusListQuaestion
        });
    }
    render() {
        const { showModal, statusListQuaestion } = this.state;

        return (
            <div className="test-page">
                <Header />

                <section className="main-content bg-f3">
                    <div className="test-detail test-detail-pc active-pc">
                        <div className="container-fluid">
                            <div className="main-content__child">
                                <div className="row">
                                    <div className="col-lg-2 aside aside-left">
                                    </div>
                                    <div className="col-lg-8 test-content-common test-content-common-1">
                                        <div className="test-part-common">
                                            <SecHead
                                                heading="Part IV: Question - Response"
                                                subHeading="Look at the picture and listen to the sentences. Choose the
                                                sentence that best describes the picture:"
                                            />
                                            <div className="sec-body sec-body-common-2">
                                                <div className="box-audio">
                                                    <audio controls>
                                                        <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/ogg" />
                                                        <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/mpeg" />
                                                        Your browser does not support the audio element.
                                                    </audio>
                                                </div>
                                                <div className="box-question">
                                                    <div className="box-question__title">
                                                        Question 60:
                                                    <span>What did the man recently do?</span>
                                                    </div>
                                                    <div className="box-question__content">
                                                        <ul className="list-question">
                                                            <li>
                                                                <div className="item check-true">
                                                                    <div className="item__choice">A</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet.</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item check-false">
                                                                    <div className="item__choice">B</div>
                                                                    <div className="item__content">Lorem ipsum dolor, sit amet
                            consectetur adipisicing elit. Pariatur, adipisci!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">C</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                            consectetur adipisicing elit. Sint, aliquid!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">D</div>
                                                                    <div className="item__content">
                                                                        <p>Lorem ipsum dolor sit amet
                                                                        consectetur adipisicing elit. In doloremque facere
                                                                        atque?
                                                                        Autem modi ducimus deserunt. Earum nulla voluptatem
                                                                        labore,
                                                                        voluptates necessitatibus blanditiis maiores
                                                                        provident
                              excepturi illum distinctio. Provident, nemo!</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="box-question">
                                                    <div className="box-question__title">
                                                        Question 61:
                    <span>What did the man recently do?</span>
                                                    </div>
                                                    <div className="box-question__content">
                                                        <ul className="list-question">
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">A</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet.</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">B</div>
                                                                    <div className="item__content">Lorem ipsum dolor, sit amet
                            consectetur adipisicing elit. Pariatur, adipisci!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">C</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                            consectetur adipisicing elit. Sint, aliquid!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">D</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                                                                    consectetur adipisicing elit. Optio et, sunt at ipsam
                            incidunt consequuntur!</div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="box-question">
                                                    <div className="box-question__title">
                                                        Question 62:
                    <span>What did the man recently do?</span>
                                                    </div>
                                                    <div className="box-question__content">
                                                        <ul className="list-question">
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">A</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet.</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">B</div>
                                                                    <div className="item__content">Lorem ipsum dolor, sit amet
                            consectetur adipisicing elit. Pariatur, adipisci!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">C</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                            consectetur adipisicing elit. Sint, aliquid!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">D</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                                                                    consectetur adipisicing elit. Optio et, sunt at ipsam
                            incidunt consequuntur!</div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2 aside aside-right">
                                        <div className>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="test-detail test-detail-mb active-mb">
                        <div className="container">
                            <div className="test-detail-mb-content">
                                <ul className="nav nav-tabs">
                                    <li className="active"><a data-toggle="tab" href="#testContent">Bài Test</a></li>
                                    <li><a data-toggle="tab" href="#tableQuestionMb">Bảng câu hỏi</a></li>
                                    <li><a data-toggle="tab" href="#viewPart">Xem các part</a></li>
                                </ul>
                                <div className="tab-content">
                                    <div id="testContent" className="tab-pane fade in active">
                                        <div className="test-part-common">
                                            <SecHead
                                                heading="Part IV: Question - Response"
                                                subHeading="Look at the picture and listen to the sentences. Choose the
                                                sentence that best describes the picture:"
                                            />
                                            <div className="sec-body sec-body-common-2">
                                                <div className="box-audio">
                                                    <audio controls>
                                                        <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/ogg" />
                                                        <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/mpeg" />
                    Your browser does not support the audio element.
                  </audio>
                                                </div>
                                                <div className="box-question">
                                                    <div className="box-question__title">
                                                        Question 60:
                    <span>What did the man recently do?</span>
                                                    </div>
                                                    <div className="box-question__content">
                                                        <ul className="list-question">
                                                            <li>
                                                                <div className="item check-true">
                                                                    <div className="item__choice">A</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet.</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item check-false">
                                                                    <div className="item__choice">B</div>
                                                                    <div className="item__content">Lorem ipsum dolor, sit amet
                            consectetur adipisicing elit. Pariatur, adipisci!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">C</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                            consectetur adipisicing elit. Sint, aliquid!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">D</div>
                                                                    <div className="item__content">
                                                                        <p>Lorem ipsum dolor sit amet
                                                                        consectetur adipisicing elit. In doloremque facere
                                                                        atque?
                                                                        Autem modi ducimus deserunt. Earum nulla voluptatem
                                                                        labore,
                                                                        voluptates necessitatibus blanditiis maiores
                                                                        provident
                              excepturi illum distinctio. Provident, nemo!</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="box-question">
                                                    <div className="box-question__title">
                                                        Question 61:
                    <span>What did the man recently do?</span>
                                                    </div>
                                                    <div className="box-question__content">
                                                        <ul className="list-question">
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">A</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet.</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">B</div>
                                                                    <div className="item__content">Lorem ipsum dolor, sit amet
                            consectetur adipisicing elit. Pariatur, adipisci!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">C</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                            consectetur adipisicing elit. Sint, aliquid!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">D</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                                                                    consectetur adipisicing elit. Optio et, sunt at ipsam
                            incidunt consequuntur!</div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="box-question">
                                                    <div className="box-question__title">
                                                        Question 62:
                    <span>What did the man recently do?</span>
                                                    </div>
                                                    <div className="box-question__content">
                                                        <ul className="list-question">
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">A</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet.</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">B</div>
                                                                    <div className="item__content">Lorem ipsum dolor, sit amet
                            consectetur adipisicing elit. Pariatur, adipisci!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">C</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                            consectetur adipisicing elit. Sint, aliquid!</div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div className="item">
                                                                    <div className="item__choice">D</div>
                                                                    <div className="item__content">Lorem ipsum dolor sit amet
                                                                    consectetur adipisicing elit. Optio et, sunt at ipsam
                            incidunt consequuntur!</div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tableQuestionMb" className="tab-pane fade">
                                        <div className="table-question">
                                            <div className="content">
                                                <ul className="list-number">
                                                    <li><span className="checked">1</span></li>
                                                    <li><span>2</span></li>
                                                    <li><span>3</span></li>
                                                    <li><span>4</span></li>
                                                    <li><span>5</span></li>
                                                    <li><span>6</span></li>
                                                    <li><span>7</span></li>
                                                    <li><span>8</span></li>
                                                    <li><span>9</span></li>
                                                    <li><span>10</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>20</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>30</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>40</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>50</span></li>
                                                    <li><span>1</span></li>
                                                    <li><span>2</span></li>
                                                    <li><span>3</span></li>
                                                    <li><span>4</span></li>
                                                    <li><span>5</span></li>
                                                    <li><span>6</span></li>
                                                    <li><span>7</span></li>
                                                    <li><span>8</span></li>
                                                    <li><span>9</span></li>
                                                    <li><span>60</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>70</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>80</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>90</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>100</span></li>
                                                    <li><span>1</span></li>
                                                    <li><span>2</span></li>
                                                    <li><span>3</span></li>
                                                    <li><span>4</span></li>
                                                    <li><span>5</span></li>
                                                    <li><span>6</span></li>
                                                    <li><span>7</span></li>
                                                    <li><span>8</span></li>
                                                    <li><span>9</span></li>
                                                    <li><span>10</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>20</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>30</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>40</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>50</span></li>
                                                    <li><span>1</span></li>
                                                    <li><span>2</span></li>
                                                    <li><span>3</span></li>
                                                    <li><span>4</span></li>
                                                    <li><span>5</span></li>
                                                    <li><span>6</span></li>
                                                    <li><span>7</span></li>
                                                    <li><span>8</span></li>
                                                    <li><span>9</span></li>
                                                    <li><span>60</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>70</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>80</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>90</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>25</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>111</span></li>
                                                    <li><span>200</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="viewPart" className="tab-pane fade">
                                        <div className="aside-road">
                                            <div className="road-title" data-toggle="collapse" data-target="#listingTest">LISTENING
                TEST</div>
                                            <div className="road-content" id="listingTest">
                                                <ul className="list-part">
                                                    <li>
                                                        <div className="part-detail">
                                                            <div className="title">Part I
                      </div>
                                                            <div className="sub-title">Picture Description</div>
                                                            <div className="item-line-height" />
                                                            <div className="item-circle">
                                                                <i className="fa fa-check-circle checked" aria-hidden="true" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="part-detail">
                                                            <div className="title">Part II
                      </div>
                                                            <div className="sub-title">Question - Response</div>
                                                            <div className="item-line-height" />
                                                            <div className="item-circle">
                                                                <i className="fa fa-check-circle checked" aria-hidden="true" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="part-detail">
                                                            <div className="title">Part III
                      </div>
                                                            <div className="sub-title">Short Conversations</div>
                                                            <div className="item-line-height" />
                                                            <div className="item-circle" />
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="part-detail">
                                                            <div className="title">Part IV
                      </div>
                                                            <div className="sub-title">Short Talks</div>
                                                            <div className="item-line-height" />
                                                            <div className="item-circle" />
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="aside-road">
                                            <div className="road-title" data-toggle="collapse" data-target="#readingTest">READING
                TEST</div>
                                            <div className="road-content fade" id="readingTest">
                                                <ul className="list-part">
                                                    <li>
                                                        <div className="part-detail">
                                                            <div className="title">Part V
                      </div>
                                                            <div className="sub-title">Picture Description</div>
                                                            <div className="item-line-height" />
                                                            <div className="item-circle">
                                                                <i className="fa fa-check-circle checked" aria-hidden="true" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="part-detail">
                                                            <div className="title">Part VI
                      </div>
                                                            <div className="sub-title">Picture Description</div>
                                                            <div className="item-line-height" />
                                                            <div className="item-circle">
                                                                <i className="fa fa-check-circle checked" aria-hidden="true" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="part-detail">
                                                            <div className="title">Part VII
                      </div>
                                                            <div className="sub-title">Picture Description</div>
                                                            <div className="item-line-height" />
                                                            <div className="item-circle" />
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="boxTime" className="box-time box-time-pc active-pc">
                    <div className="container-fluid">
                        <div className="row row-mg-0">
                            <div className="col-lg-2 col-pd-0" />
                            <div className="col-lg-8 col-pd-0">
                                <div className="box-time-content">
                                    <div className="box-time-content__left">
                                        <div className="box-button">
                                            <button id="buttonClickShowTableQuestion" type="button" className="btn btn-primary ml0" onClick={this.onHandleToggle}>Bảng
                                                câu hỏi</button>
                                        </div>
                                    </div>
                                    <div className="box-time-content__center">
                                        <i className="fa fa-clock-o" aria-hidden="true" />
                                        <span id="ten-countdown" />
                                    </div>
                                    <div className="box-time-content__right">
                                        <div className="box-button">
                                            <a className="btn btn-cyan">Next Question<i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                                            <Link to="/part-5" className="btn btn-cyan">Next Part<i className="fa fa-arrow-circle-right" aria-hidden="true" /></Link>
                                            <Button className="btn btn-default" onClick={this.openModal}>
                                                Nộp bài
                                            </Button>
                                        </div>
                                    </div>
                                    <div id="tableQuestion" className={statusListQuaestion === false ? 'table-question dp-none' : 'table-question dp-block'} >
                                        <div className="title">Bảng câu hỏi</div>
                                        <div className="content">
                                            <ul className="list-number">
                                                <li><span className="checked">1</span></li>
                                                <li><span>2</span></li>
                                                <li><span>3</span></li>
                                                <li><span>4</span></li>
                                                <li><span>5</span></li>
                                                <li><span>6</span></li>
                                                <li><span>7</span></li>
                                                <li><span>8</span></li>
                                                <li><span>9</span></li>
                                                <li><span>10</span></li>
                                                <li><span>11</span></li>
                                                <li><span>12</span></li>
                                                <li><span>13</span></li>
                                                <li><span>14</span></li>
                                                <li><span>15</span></li>
                                                <li><span>16</span></li>
                                                <li><span>17</span></li>
                                                <li><span>18</span></li>
                                                <li><span>19</span></li>
                                                <li><span>20</span></li>
                                                <li><span>21</span></li>
                                                <li><span>22</span></li>
                                                <li><span>23</span></li>
                                                <li><span>24</span></li>
                                                <li><span>25</span></li>
                                                <li><span>26</span></li>
                                                <li><span>27</span></li>
                                                <li><span>28</span></li>
                                                <li><span>29</span></li>
                                                <li><span>30</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2" />
                        </div>
                    </div>
                </section>

                <section id="boxTimeMb" className="box-time box-time-mb active-mb">
                    <div className="box-time-mb-content">
                        <div className="time-total">
                            <i className="fa fa-clock-o" aria-hidden="true" />
                            <span id="ten-countdown-mb" />
                        </div>
                        <div className="box-button">
                            <a className="btn btn-cyan">Next Question<i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                            <Link to="/part-5" className="btn btn-cyan">Next Part<i className="fa fa-arrow-circle-right" aria-hidden="true" /></Link>
                            <Button className="btn btn-default" onClick={this.openModal}>Nộp bài</Button>
                        </div>
                    </div>
                </section>

                <Modal show={showModal} onHide={this.closeModal} className="modal-submit">
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <button type="button" className="close"></button>
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div>
                            <div className="title">Chức mừng bạn đã hoàn thành bài thi</div>
                            <div className="sub-title">Interview Skills Test 1</div>
                            <p className="text">Điểm của ban</p>
                            <div className="circle">
                                <span className="point">500/990</span>
                            </div>
                            <p className="text">Đáp án đúng: 0 / 30</p>
                            <p className="text">Số câu hoàn thành: 0 / 30 câu</p>
                            <div className="box-button">
                                {/* <a className="btn btn-blue-ebomb">Nhận tư vấn lộ trình học</a> */}
                                {/* <a className="btn btn-blue-ebomb">Làm lại bài test</a> */}
                                <Link to="/fulltest" className="btn btn-blue-ebomb">Làm lại bài test</Link>
                                {/* <a className="btn btn-blue-ebomb">Xem đáp án</a> */}
                            </div>
                        </div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.closeModal}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        )
    }
}
