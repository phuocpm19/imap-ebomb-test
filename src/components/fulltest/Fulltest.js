import React, { Component } from 'react'
import Header from '../shared/Header'
import Footer from '../shared/Footer'
import { Link } from 'react-router-dom'
import ListPart from '../shared/ListPart'

export default class Fulltest extends Component {
    render() {
        return (
            <div className="test-page">
                <Header />

                <section className="main-content bg-f3">
                    <div className="container-fluid">
                        <div className="main-content__child">
                            <div className="row">
                                <div className="col-lg-2 active-pc aside aside-left">
                                </div>
                                <div className="col-lg-8 col-xs-12 test-content-common test-content-common-2">
                                    <div className="test-content-common-child">
                                        <p className="text text-normal">Trải nghiệm cảm giác thi TOEIC với bài kiểm tra mới</p>
                                        <p className="text text-big">Final Test - Practice Full Test TOEIC Reading, Listening 7</p>
                                        <p className="text text-middle">Thời gian làm bài: 120 phút</p>
                                        <p className="text text-middle">Listening: 45 phút</p>
                                        <p className="text text-middle">Reading: 75 phút</p>
                                        <p className="text text-normal mt40 mb10">Dựa trên cấu trúc đề thi TOEIC, bài kiểm tra này
                                        có tổng cộng 7 phần thi, bao gồm 4 phần Nghe và 3 phần Đọc. </p>
                                        <p className="text text-normal">Đây là một bài mẫu giúp bạn chuẩn bị cho kì thi TOEIC</p>
                                        <div className="box-button">
                                            <Link to="/part-1" className="btn btn-cyan">Bắt đầu ngay</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-2 active-pc aside aside-right">
                                    <ListPart />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <Footer />
            </div>
        )
    }
}
