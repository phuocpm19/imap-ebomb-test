import React, { Component } from 'react'
import Header from '../shared/Header'
import SecHead from '../shared/SecHead'
import { Link } from 'react-router-dom'
import { Button, Modal, Tab, Tabs } from 'react-bootstrap'
import HeaderMobile from '../shared/HeaderMobile'
import QuestionItem from '../shared/QuestionItem'
import TabListQuestion from '../shared/TabListQuestion'
import ListPart from '../shared/ListPart'

export default class Part2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: false,
            keyTabs: 'home',
            statusListQuaestion: false,
            listQuestionPartTwo: [
                {
                    id: 1,
                    itemChoice: 'A',
                    statusChoice: false,
                    statusCheck: false
                },
                {
                    id: 2,
                    itemChoice: 'B',
                    statusChoice: false,
                    statusCheck: false
                },
                {
                    id: 3,
                    itemChoice: 'C',
                    statusChoice: false,
                    statusCheck: false
                },
                {
                    id: 4,
                    itemChoice: 'D',
                    statusChoice: false,
                    statusCheck: false
                },
            ]
        }
    }

    closeModal = () => {
        this.setState({
            showModal: false
        });
    }

    openModal = () => {
        this.setState({
            showModal: true
        });
    }

    onHandleToggle = () => {
        this.setState({
            statusListQuaestion: !this.state.statusListQuaestion
        });
    }

    render() {
        const { listQuestionPartTwo } = this.state;
        const { showModal, statusListQuaestion } = this.state;

        let elmQuestionPartTwo = listQuestionPartTwo.map((item, index) => {
            return <QuestionItem
                key={index}
                itemChoice={item.itemChoice}
                statusChoice={item.statusChoice}
            />
        })

        return (
            <div className="test-page">
                <Header />

                <HeaderMobile />

                <section className="main-content bg-f3">
                    <div className="test-detail test-detail-pc active-pc">
                        <div className="container-fluid">
                            <div className="main-content__child">
                                <div className="row">
                                    <div className="col-lg-2 aside aside-left">
                                    </div>
                                    <div className="col-lg-8 test-content-common test-content-common-1">
                                        <div className="test-part-common">
                                            <SecHead
                                                heading="Part II: Question - Response"
                                                subHeading="Look at the picture and listen to the sentences. 
                                                Choose the sentence that best describes the picture:"
                                            />
                                            <div className="sec-body sec-body-common-2">
                                                <div className="box-audio">
                                                    <audio controls>
                                                        <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/ogg" />
                                                        <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/mpeg" />
                                                            Your browser does not support the audio element.
                                                        </audio>
                                                </div>
                                                <div className="box-question">
                                                    <div className="box-question__title">
                                                        Question 10:
                                                    </div>
                                                    <div className="box-question__content">
                                                        <ul className="list-question">
                                                            {elmQuestionPartTwo}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2 aside aside-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="test-detail test-detail-mb active-mb">
                        <div className="container">
                            <div className="test-detail-mb-content">
                                <div className="r-tab-custom">
                                    <Tabs
                                        id="controlled-tab-example"
                                        activeKey={this.state.keyTabs}
                                        onSelect={keyTabs => this.setState({ keyTabs })}
                                        className="r-tab-list"
                                    >
                                        <Tab eventKey="home" title="Bài Test" className="r-tab-item">
                                            <div className="r-tab-content tab-1">
                                                <div className="test-part-common">
                                                    <SecHead
                                                        heading="Part I: Picture Description"
                                                        subHeading="Look at the picture and listen to the sentences. Choose
                                                        the sentence that best describes the picture:"
                                                    />
                                                    <div className="sec-body sec-body-common-2">
                                                        <div className="box-picture">
                                                            <div className="image img-scaledown">
                                                                <img src="img/TEST-part-1.png" alt="img question" />
                                                            </div>
                                                        </div>
                                                        <div className="box-audio">
                                                            <audio controls>
                                                                <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/ogg" />
                                                                <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/mpeg" />
                                                            Your browser does not support the audio element.
                                                    </audio>
                                                        </div>
                                                        <div className="box-question">
                                                            <div className="box-question__title">
                                                                Question 1:
                                                    </div>
                                                            <div className="box-question__content">
                                                                <ul className="list-question">
                                                                    {elmQuestionPartTwo}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Tab>

                                        <Tab eventKey="profile" title="Bảng câu hỏi" className="r-tab-item">
                                            <div className="r-tab-content tab-2">
                                                <TabListQuestion />
                                            </div>
                                        </Tab>

                                        <Tab eventKey="contact" title="Xem các Part" className="r-tab-item">
                                            <div className="r-tab-content tab-3">
                                                <ListPart />
                                            </div>
                                        </Tab>
                                    </Tabs>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="boxTime" className="box-time box-time-pc active-pc">
                    <div className="container-fluid">
                        <div className="row row-mg-0">
                            <div className="col-lg-2 col-pd-0" />
                            <div className="col-lg-8 col-pd-0">
                                <div className="box-time-content">
                                    <div className="box-time-content__left">
                                        <div className="box-button">
                                            <button id="buttonClickShowTableQuestion" type="button" className="btn btn-primary ml0" onClick={this.onHandleToggle}>Bảng
                                                câu hỏi</button>
                                        </div>
                                    </div>
                                    <div className="box-time-content__center">
                                        <i className="fa fa-clock-o" aria-hidden="true" />
                                        <span id="ten-countdown" />
                                    </div>
                                    <div className="box-time-content__right">
                                        <div className="box-button">
                                            <a className="btn btn-cyan">Next Question<i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                                            <Link to="/part-3" className="btn btn-cyan">Next Part<i className="fa fa-arrow-circle-right" aria-hidden="true" /></Link>
                                            <Button className="btn btn-default" onClick={this.openModal}>
                                                Nộp bài
                                            </Button>
                                        </div>
                                    </div>
                                    <div id="tableQuestion" className={statusListQuaestion === false ? 'table-question dp-none' : 'table-question dp-block'} >
                                        <div className="title">Bảng câu hỏi</div>
                                        <div className="content">
                                            <ul className="list-number">
                                                <li><span className="checked">1</span></li>
                                                <li><span>2</span></li>
                                                <li><span>3</span></li>
                                                <li><span>4</span></li>
                                                <li><span>5</span></li>
                                                <li><span>6</span></li>
                                                <li><span>7</span></li>
                                                <li><span>8</span></li>
                                                <li><span>9</span></li>
                                                <li><span>10</span></li>
                                                <li><span>11</span></li>
                                                <li><span>12</span></li>
                                                <li><span>13</span></li>
                                                <li><span>14</span></li>
                                                <li><span>15</span></li>
                                                <li><span>16</span></li>
                                                <li><span>17</span></li>
                                                <li><span>18</span></li>
                                                <li><span>19</span></li>
                                                <li><span>20</span></li>
                                                <li><span>21</span></li>
                                                <li><span>22</span></li>
                                                <li><span>23</span></li>
                                                <li><span>24</span></li>
                                                <li><span>25</span></li>
                                                <li><span>26</span></li>
                                                <li><span>27</span></li>
                                                <li><span>28</span></li>
                                                <li><span>29</span></li>
                                                <li><span>30</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2" />
                        </div>
                    </div>
                </section>

                <section id="boxTimeMb" className="box-time box-time-mb active-mb">
                    <div className="box-time-mb-content">
                        <div className="time-total">
                            <i className="fa fa-clock-o" aria-hidden="true" />
                            <span id="ten-countdown-mb" />
                        </div>
                        <div className="box-button">
                            <a className="btn btn-cyan">Next Question<i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                            <Link to="/part-3" className="btn btn-cyan">Next Part<i className="fa fa-arrow-circle-right" aria-hidden="true" /></Link>
                            <Button className="btn btn-default" onClick={this.openModal}>Nộp bài</Button>
                        </div>
                    </div>
                </section>

                <Modal show={showModal} onHide={this.closeModal} className="modal-submit">
                    <Modal.Header closeButton>
                        <Modal.Title>
                            <button type="button" className="close"></button>
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div>
                            <div className="title">Chức mừng bạn đã hoàn thành bài thi</div>
                            <div className="sub-title">Interview Skills Test 1</div>
                            <p className="text">Điểm của ban</p>
                            <div className="circle">
                                <span className="point">500/990</span>
                            </div>
                            <p className="text">Đáp án đúng: 0 / 30</p>
                            <p className="text">Số câu hoàn thành: 0 / 30 câu</p>
                            <div className="box-button">
                                {/* <a className="btn btn-blue-ebomb">Nhận tư vấn lộ trình học</a> */}
                                {/* <a className="btn btn-blue-ebomb">Làm lại bài test</a> */}
                                <Link to="/fulltest" className="btn btn-blue-ebomb">Làm lại bài test</Link>
                                {/* <a className="btn btn-blue-ebomb">Xem đáp án</a> */}
                            </div>
                        </div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.closeModal}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        )
    }
}
