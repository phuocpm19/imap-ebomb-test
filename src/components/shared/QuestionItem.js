import React, { Component } from 'react'

export default class QuestionItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // statusChoice: this.props.statusChoice
            listChoice: this.props.listChoice
        }
        console.log(this.state.listChoice);


        // console.log(this.state.statusChoice);
    }
    onHandleChoice = () => {
        // let statusChoice = this.props.statusChoice;
        // statusChoice = !statusChoice;
        // console.log(statusChoice);
        this.setState({
            statusChoice: !this.state.statusChoice
        })
        // console.log('click choice');
        // console.log(this.state.statusChoice);
        // this.props.statusChoice = !this.props.statusChoice
        // console.log(this.props.statusChoice);
    }
    render() {
        const { itemChoice, iTemContent } = this.props;
        // let { itemChoice, iTemContent, statusChoice } = this.props;
        const { statusChoice } = this.state;
        return (
            <li>
                <div className="item">
                    <div className={statusChoice === false ? 'item__choice' : 'item__choice item-choiced'} onClick={this.onHandleChoice}>{itemChoice}</div>
                    <div className="item__content" onClick={this.onHandleChoice}>{iTemContent}</div>
                </div>
            </li>
        )
    }
}
