import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class ListPart extends Component {
    render() {
        return (
            <>
                <div className="aside-road">
                    <div className="road-title">LISTENING TEST</div>
                    <div className="road-content">
                        <ul className="list-part">
                            <li>
                                <div className="part-detail">
                                    <Link to="/part-1" className="title">Part I</Link>
                                    <Link to="/part-1" className="sub-title">Picture Description</Link>
                                    <div className="item-line-height" />
                                    <div className="item-circle">
                                        {/* <i className="fa fa-check-circle checked" aria-hidden="true" /> */}
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="part-detail">
                                    <Link to="/part-2" className="title">Part II</Link>
                                    <Link to="/part-2" className="sub-title">Question - Response</Link>
                                    <div className="item-line-height" />
                                    <div className="item-circle">
                                        {/* <i className="fa fa-check-circle checked" aria-hidden="true" /> */}
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="part-detail">
                                    <Link to="/part-3" className="title">Part III</Link>
                                    <Link to="/part-3" className="sub-title">Short Conversations</Link>
                                    <div className="item-line-height" />
                                    <div className="item-circle" />
                                </div>
                            </li>
                            <li>
                                <div className="part-detail">
                                    <Link to="/part-4" className="title">Part IV</Link>
                                    <Link to="/part-4" className="sub-title">Short Talks</Link>
                                    <div className="item-line-height" />
                                    <div className="item-circle" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="aside-road">
                    <div className="road-title">READING TEST</div>
                    <div className="road-content">
                        <ul className="list-part">
                            <li>
                                <div className="part-detail">
                                    <Link to="/part-5" className="title">Part V</Link>
                                    <Link to="/part-5" className="sub-title">Picture Description</Link>
                                    <div className="item-line-height" />
                                    <div className="item-circle">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="part-detail">
                                    <Link to="/part-6" className="title">Part VI</Link>
                                    <Link to="/part-6" className="sub-title">Picture Description</Link>
                                    <div className="item-line-height" />
                                    <div className="item-circle">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="part-detail">
                                    <Link to="/part-7" className="title">Part VII</Link>
                                    <Link to="/part-7" className="sub-title">Picture Description</Link>
                                    <div className="item-line-height" />
                                    <div className="item-circle" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </>
        )
    }
}
