import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class Header extends Component {
    render() {
        return (
            <header className="header-pc active-pc">
                <div className="container-fluid">
                    <div className="header-pc-content">
                        <div className="header-pc-content__logo">
                            <div className="thumb">
                                <a href="https://ebomb.edu.vn/" title="Trang chu ebomb" className="image img-scaledown">
                                    <img src="img/logo.png" alt="logo ebomb" />
                                </a>
                            </div>
                        </div>
                        <div className="header-pc-content__toolbox">
                            <div className="box-button">
                                <Link to="/fulltest" className="btn btn-default">Xem các Part</Link>
                            </div>
                            <Link to="/user-account" className="box-info">
                                <div className="info-ava image img-cover">
                                    <img src="img/avatar.jpg" alt="avatar user" />
                                </div>
                                <div className="info-user">Hoa Nguyen</div>
                            </Link>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}
