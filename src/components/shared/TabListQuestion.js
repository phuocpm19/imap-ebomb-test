import React, { Component } from 'react'

export default class TabListQuestion extends Component {
    render() {
        return (
            <div className="table-question">
                <div className="content">
                    <ul className="list-number">
                        <li><span className="checked">1</span></li>
                        <li><span>2</span></li>
                        <li><span>2</span></li>
                        <li><span>2</span></li>
                        <li><span>2</span></li>
                        <li><span>2</span></li>
                        <li><span>2</span></li>
                        <li><span>2</span></li>
                    </ul>
                </div>
            </div>
        )
    }
}
