import React, { Component } from 'react'
import $ from "jquery";
import { Link } from 'react-router-dom';
window.jQuery = $;
window.$ = $;
export default class HeaderMobile extends Component {
    // constructor() {
    //     super();
    //     this._handleClick = this._handleClick.bind(this);
    // }
    componentDidMount = () => {
        this._handleClick();
    }

    _handleClick() {
        $(function () {
            function slideMenu() {
                var activeState = $("#menu-container .menu-list").hasClass("active");
                $("#menu-container .menu-list").animate({ left: activeState ? "0%" : "-100%" }, 400);
            }

            $("#menu-wrapper").click(function (event) {
                event.stopPropagation();
                $("#hamburger-menu").toggleClass("open");
                $("#menu-container .menu-list").toggleClass("active");
                slideMenu();

                $("body").toggleClass("overflow-hidden");
            });

            $(".menu-list").find(".accordion-toggle").click(function () {
                $(this).next().toggleClass("open").slideToggle("fast");
                $(this).toggleClass("active-tab").find(".menu-link").toggleClass("active");

                $(".menu-list .accordion-content").not($(this).next()).slideUp("fast").removeClass("open");
                $(".menu-list .accordion-toggle").not($(this)).removeClass("active-tab").find(".menu-link").removeClass("active");
            });
        });
    }
    render() {
        return (
            <header className="header-mb active-mb">
                <header className="header-mb active-mb">
                    <div id="menu-container">
                        <div className="panel-head-mb">
                            <div id="menu-wrapper">
                                <div id="hamburger-menu">
                                    <span />
                                    <span />
                                    <span />
                                </div>
                            </div>
                            <div className="logo-mb">
                                <a href="https://ebomb.edu.vn/" title="trang chu ebomb" className="image img-cover">
                                    <img src="/img/logo-mb.png" alt="logo ebomb" />
                                </a>
                            </div>
                            {/* <a className="button button-default dp-none">Đăng nhập</a> */}
                            <div className="logout-mb dp-block">
                                <div className="logout-content">
                                    <div className="avatar">
                                        <Link to="/user-account" className="image img-scaledown">
                                            <img src="/img/avatar2.jpg" alt="avatar user" />
                                        </Link>
                                    </div>
                                    {/* <div className="box-logout">
                                        <a href className title>Log out</a>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                        <div className="panel-body-mb">
                            <ul className="menu-list accordion">
                                <li className="search-mb">
                                    <svg id="_24px_33_" data-name="24px (33)" xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24">
                                        <path id="Path_416" data-name="Path 416" d="M15.5,14h-.79l-.28-.27a6.51,6.51,0,1,0-.7.7l.27.28v.79l5,4.99L20.49,19Zm-6,0A4.5,4.5,0,1,1,14,9.5,4.494,4.494,0,0,1,9.5,14Z" fill="#575757" />
                                        <path id="Path_417" data-name="Path 417" d="M0,0H24V24H0Z" fill="none" />
                                    </svg>
                                    <input type="text" placeholder="Tìm kiếm" className="input-search active-mb" />
                                </li>
                                <li id="nav1" className="toggle menu-active-mb">
                                    <a className="menu-link" href="#">Trang chủ</a>
                                </li>
                                <li id="nav3" className="toggle">
                                    <a className="menu-link" href="#">Về chúng tôi</a>
                                </li>
                                <li id="nav2" className="toggle accordion-toggle">
                                    <a className="menu-link" href="#">Khóa học</a>
                                </li>
                                <ul className="menu-submenu accordion-content">
                                    <li><a className="head" href="#">Khóa học Online</a></li>
                                    <li><a className="head" href="#">Khóa học Offline</a></li>
                                </ul>
                                <li id="nav3" className="toggle">
                                    <a className="menu-link" href="#">Test Online</a>
                                </li>
                                <li id="nav3" className="toggle">
                                    <a className="menu-link" href="#">Video</a>
                                </li>
                                <li id="nav3" className="toggle">
                                    <a className="menu-link" href="#">Tin tức</a>
                                </li>
                                <li id="nav3" className="toggle">
                                    <a className="menu-link" href="#">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </header>
            </header>

        )
    }
}
