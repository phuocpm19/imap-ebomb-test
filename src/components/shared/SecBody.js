import React, { Component } from 'react'

export default class SecBody extends Component {
    render() {
        return (
            <div className="sec-body sec-body-common-2">
                <div className="box-picture">
                    <div className="image img-scaledown">
                        <img src="img/TEST-part-1.png" alt="img question" />
                        <img src="img/TEST-part-1.png" alt="img question" />
                    </div>
                </div>
                <div className="box-audio">
                    <audio controls>
                        <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/ogg" />
                        <source src="img/Payphone - Maroon 5_ Wiz Khalifa.m4a" type="audio/mpeg" />
                                                            Your browser does not support the audio element.
                                                    </audio>
                </div>
                <div className="box-question">
                    <div className="box-question__title">
                        Question 1:
                                                    </div>
                    <div className="box-question__content">
                        <ul className="list-question">
                            {/* {elmQuestionPartOne} */}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
