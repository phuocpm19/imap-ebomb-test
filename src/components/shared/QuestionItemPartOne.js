import React, { Component } from 'react'
import QuestionItem from './QuestionItem';

export default class QuestionItemPartOne extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         listChoice: this.props.listChoice
    //     }
    //     console.log(this.state.listChoice);
    // }

    render() {
        let { id, srcImg, srcAudio, listChoice } = this.props;
        return (
            // <div className="sec-body sec-body-common-2">
            <div className="box-question">
                <div className="box-question__title">
                    Question {id}:
                    </div>

                <div className="box-picture">
                    <div className="image img-scaledown">
                        <img src={srcImg} alt="img question" />
                    </div>
                </div>

                <div className="box-audio">
                    <audio controls>
                        <source src={srcAudio} type="audio/ogg" />
                        <source src={srcAudio} type="audio/mpeg" />
                                Your browser does not support the audio element.
                        </audio>
                </div>
                <div className="box-question__content">
                    <ul className="list-question">
                        <QuestionItem
                            listChoice={listChoice}
                        />
                    </ul>
                </div>




            </div>
            // </div>
        )
    }
}
