import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Part1 from './components/fulltest/Part1'
import Part2 from './components/fulltest/Part2'
import Part3 from './components/fulltest/Part3'
import Part4 from './components/fulltest/Part4'
import Part5 from './components/fulltest/Part5'
import Part6 from './components/fulltest/Part6'
import Part7 from './components/fulltest/Part7'
import Fulltest from './components/fulltest/Fulltest'
import TestOnline from './components/TestOnline'
import Result from './components/shared/Result'
import UserAccount from './components/pages/UserAccount'

export default class App extends Component {
    render() {
        return (
            // <p>aaaaa</p>
            <Router>
                <Switch>
                    <Route path="/part-1">
                        <Part1 />
                    </Route>
                    <Route path="/part-2">
                        <Part2 />
                    </Route>
                    <Route path="/part-3">
                        <Part3 />
                    </Route>
                    <Route path="/part-4">
                        <Part4 />
                    </Route>
                    <Route path="/part-5">
                        <Part5 />
                    </Route>
                    <Route path="/part-6">
                        <Part6 />
                    </Route>
                    <Route path="/part-7">
                        <Part7 />
                    </Route>
                    <Route path="/user-account">
                        <UserAccount />
                    </Route>
                    <Route path="/fulltest">
                        <Fulltest />
                    </Route>
                    <Route path="/">
                        <TestOnline />
                    </Route>
                </Switch>
            </Router>
        )
    }
}
